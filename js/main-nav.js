document.querySelectorAll('.js-main-nav-li').forEach(el => {
    el.addEventListener('mouseenter', () => {
        document.querySelectorAll('.js-main-nav-li').forEach(li => li.classList.remove('c-main-nav__li--active'));
        el.classList.add('c-main-nav__li--active');
    });
});

document.querySelectorAll('.js-main-nav-li a').forEach(el => {
    // a에 포커스가 오면 가장 가까운 .js-main-nav-li를 찾아서 c-main-nav__li--active 클래스를 붙인다.
    el.addEventListener('focus', () => {
        document.querySelectorAll('.js-main-nav-li').forEach(li => li.classList.remove('c-main-nav__li--active'));
        el.closest('.js-main-nav-li').classList.add('c-main-nav__li--active');
    });
});