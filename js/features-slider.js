import Swiper from './swiper/swiper.min.mjs';
import Navigation from './swiper/modules/navigation.min.mjs';
import Pagination from './swiper/modules/pagination.min.mjs';




if (document.querySelector('.js-feature-slider')) {
  const removeMyStyle = function () {
    document.querySelector('.js-feature-slider-wrapper').classList.remove('u-overflow-x-scroll');
    document.querySelector('.js-feature-slider-wrapper').classList.remove('c-feature-slider__wrapper');
    document.querySelectorAll('.js-feature-slider-wrapper  [class*="c-feature-slider"]').forEach(el => {
      el.classList.forEach(name => {
        if (/c-feature-slider/.test(name)) {
          el.classList.remove(name);
        }
      });
    });
  };

  const debounce = function (func, timeout = 300) {
    let timer;
    return (...args) => {
      clearTimeout(timer);
      timer = setTimeout(() => { func.apply(this, args); }, timeout);
    };
  };

  const init = debounce(function () {

    removeMyStyle();
    
    if (window.featureSlider) {
      window.featureSlider.destroy();
    }

    const slidesPerGroup = Math.max(4, Math.min(6, Math.floor(window.innerWidth / 150)));
    const slidesPerView = (window.innerWidth < 980) ? slidesPerGroup + 0.5 : slidesPerGroup;

    window.featureSlider = new Swiper('.js-feature-slider', {
      modules: [Navigation, Pagination],
      loop: true,
      slidesPerGroup: slidesPerGroup,
      slidesPerView: slidesPerView,
      spaceBetween: 15,
      navigation: {
        nextEl: '.js-feature-slider-navigation-next',
        prevEl: '.js-feature-slider-navigation-prev',
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
    });
  });

  window.addEventListener('resize', e => init());
  init();
}

