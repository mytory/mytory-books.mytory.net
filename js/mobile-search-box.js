document.querySelectorAll('.js-toggle-mobile-search-box').forEach(el => {
    el.addEventListener('click', (e) => {
        if (e.target.closest('.js-search-box')) {
            e.stopPropagation();
            return;
        }
        const modalEl = document.querySelector('.c-modal');
        modalEl.classList.toggle('c-modal--visible');

        if (modalEl.classList.contains('c-modal--visible')) {
            document.querySelector('#s').focus();
        } else {
            document.querySelector('#s-button').focus();
        }
    });
});