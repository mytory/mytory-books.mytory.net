document.querySelectorAll('.js-tab-button').forEach(el => {
    el.addEventListener('click', e => {
        console.log('click!');

        // tab 처리
        document.querySelector('.c-tab__button--active').classList.remove('c-tab__button--active');
        e.target.classList.add('c-tab__button--active');

        // content 처리
        const selector = e.target.getAttribute('href');
        document.querySelectorAll('.js-tab-content').forEach(el => {
            el.classList.remove('c-tab__content--active');
            el.ariaHidden = true;
        });
        document.querySelector(selector).classList.add('c-tab__content--active');
        document.querySelector(selector).ariaHidden = false;
    });
});

// 하단 내비게이션 이벤트 핸들
document.querySelectorAll('.js-bottom-nav').forEach(el => {
    el.addEventListener('click', e => {
        e.preventDefault();
        const href = e.target.getAttribute('href');
        document.querySelector(`.js-tab-button[href="${href}"]`).click();
    });
});


// 직접 입력으로 들어올 때 URL에 해시를 달고 들어오면 해당 탭이 선택돼 있게 하기
const url = new URL(location.href);
const selector = decodeURIComponent(url.hash);
if (selector) {
    document.querySelector(`[href='${selector}']`)?.click();
}
