import Swiper from './swiper/swiper.min.mjs';
import Navigation from './swiper/modules/navigation.min.mjs';

if (document.querySelector('.js-card-news')) {

    document.querySelector('.js-card-news-wrapper').classList.remove('u-overflow-x-scroll');
    document.querySelectorAll('.js-card-news-arrow').forEach(el => el.classList.remove('u-hidden'));

    window.cardNews = new Swiper('.js-card-news', {
        modules: [Navigation],
        loop: false,
        navigation: {
            prevEl: '.swiper-button-prev',
            nextEl: '.swiper-button-next',
        }
    });
}
