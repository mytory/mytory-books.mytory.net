import Swiper from './swiper/swiper.min.mjs';
import Navigation from './swiper/modules/navigation.min.mjs';

if (document.querySelector('.js-swiper-wrapper')) {
  document.querySelector('.js-swiper-wrapper').classList.remove('u-overflow-x-scroll');
  document.querySelector('.swiper-button-prev').classList.remove('u-hidden');
  document.querySelector('.swiper-button-next').classList.remove('u-hidden');

  window.swiper = new Swiper('.js-index-slider', {
    modules: [Navigation],
    loop: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
  });
}

